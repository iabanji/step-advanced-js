'use strict';

function getCards(token) {
    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
        },
    })
        .then(response => response.json())
        .then(cards => {
            console.log(cards);
            const noItemsTag = document.getElementById('noItemsTag');
            if (cards.length) {
                document.getElementById('cardItems').style.display = 'block';
                noItemsTag.style.display = 'none';
            } else {
                noItemsTag.style.display = 'block';
            }
        });
}

window.addEventListener('load', (ev) => {
    if (localStorage.getItem('token')) {
        document.getElementById('loginFormBtn').style.display = 'none';
        document.getElementById('createVisit').style.display = 'block';
        getCards(localStorage.getItem('token'));
    } else {
        document.getElementById('loginFormBtn').style.display = 'block';
        document.getElementById('createVisit').style.display = 'none';
        document.getElementById('noItemsTag').style.display = 'block';
    }
    document.getElementById('loginBtn')?.addEventListener('click', (e) => {
        e.preventDefault();
        const email = document.getElementById('email1')?.value;
        const password = document.getElementById('password1')?.value;
        if (password && email) {
            fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: email, password: password })
            })
                .then(response => {
                    if (response.status === 200) {
                        return response.text();
                    } else {
                        alert('Неправильний імейл або пароль!');
                    }
                })
                .then(token => {
                    if (token) {
                        localStorage.setItem("token", token);
                        document.getElementById('loginFormBtn').style.display = 'none';
                        document.getElementById('createVisit').style.display = 'block';
                        document.getElementById('closeLoginModalButton')?.click();
                    }
                })
        }
    });

    document.getElementById('createVisitBtn')?.addEventListener('click', (e) => {
        e.preventDefault();
        const form = document.getElementById('createVisitModalForm');

        const inputs = form.elements;

        let data = {};

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].nodeName === "INPUT" || inputs[i].nodeName === "SELECT" || inputs[i].nodeName === "TEXTAREA") {
                if (inputs[i].value) {
                    data[inputs[i].name] = inputs[i].value;
                }
            }
        }

        const token = localStorage.getItem('token');

        if (data.doctor && data.title && data.description && data.urgency && data['full_name']) {
            fetch("https://ajax.test-danit.com/api/v2/cards", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(data)
            })
                .then(response => response.json())
                .then(response => console.log(response))
        } else {
            alert("Заповніть будь ласка усі обов'язкові поля!");
        }
    });
});